﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRange : MonoBehaviour
{

    public float rangex;
    public Transform Player;

    int MoveSpeed = 6;
    int MaxDist = 10;
    int MinDist = 0;

    void Update()
    {
        if (Vector3.Distance(Player.position, transform.position) <= rangex)
        {
            {
                transform.LookAt(Player);

                if (Vector3.Distance(transform.position, Player.position) >= MinDist)
                {

                    transform.position += transform.forward * MoveSpeed * Time.deltaTime;



                    if (Vector3.Distance(transform.position, Player.position) <= MaxDist)
                    {
                        //function here
                    }
                }
            }
        }
    }
}