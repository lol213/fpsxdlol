﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMoveScript : MonoBehaviour
{
    enum Activate { OnEnter, WhileStaying }
    public Transform[] Target;
    public float Speed = 0.1f;
    Vector3 _originalPosition;
    Vector3 _targetPosition;
    int _targetIndex = 0;

    // Use this for initialization
    void Start()
    {
        _originalPosition = transform.position;
        _targetPosition = Target[_targetIndex].position;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position != _targetPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, _targetPosition, Speed);
        }
        else
        {
            _targetIndex++;
            if (_targetIndex < Target.Length)
            {
                _targetPosition = Target[_targetIndex].position;
            }
            else
            {
                _targetIndex = -1;
                _targetPosition = _originalPosition;
            }

        }
    }
}